from app.libraries.setup import Setup
from app.libraries.ejabberdapi import Ejabberd
from app.libraries.strings import Strings
from app.libraries.database import Database
from mastodon import Mastodon, StreamListener
import sys
import humanize
import datetime as dt
from datetime import datetime, date, timedelta
import pdb

def check_activity(id, username):

    active = False

    try:

        act_st = mastodon.account_statuses(id)

        active_post = 0

        for status in act_st:

            if status.created_at.replace(tzinfo=None) > datetime.now() - timedelta(days=30):

                active_post += 1

        if active_post >= 3:

            active = True

    except:

        print(f"username {username} not found.")
        pass

    return active

# main
if __name__ == '__main__':

    setup = Setup()

    mastodon = Mastodon(
        access_token = setup.mastodon_app_token,
        api_base_url= setup.mastodon_hostname
        )

    ejabberd = Ejabberd()

    strings = Strings()

    db = Database()

    users  = db.load_users()

    for user in users:

        active = check_activity(user['id'], user['username'])

        if not active:

            print(f"user {user['username']} is not active enough. Deleting it from {user['service']} and database.\n")

            is_unregistered, is_admin = ejabberd.unregister(user['username'], ejabberd.local_vhost)

            db.delete_user(user['id'], user['username'])

