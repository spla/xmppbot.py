# xmpp bot for Mastodon
Bot to manage a xmpp ejabberd node from a Mastodon server, by posting keywords to it.  

The bot only listen keywords from your Mastodon server local users. They can register themselves to your ejabberd xmpp server, unregister, check your xmpp node stats and more!.  
The keywords that they can issue against the bot are the following:  

@your_bot register  
@your_bot unregister  
@your_bot stats  
@your_bot status  
@your_bot sessions  

The bot will process any of the above keywords thanks to the wrapper for ejabberd API (`ejabberdapi.py`, coded by me (WIP)) and the excellent wrapper for Mastodon API coded by [halcy](https://github.com/halcy/Mastodon.py).  

The first time you run `python xmpp.py`, it will ask you for the needed parameters like:  

- api_base_url: http://127.0.0.1:5280 (ejabberd server's API listen port)
- local_vhost: your local ejabberd vhost  
- admin_account: the ejabberd admin account, in exemple admin@ejabberd.server  
- admin_pass: ejabberd admin account password  
- Mastodon hostname: in ex. your.mastodon.server.host  
- bot's replies language (ca or en)  

# Requirements  

- Mastodon server bot account  
- xmpp Ejabberd local node with admin privileges  

Before running `python xmpp.py`:  

1. git clone https://git.mastodont.cat/spla/xmppbot.py target_dir.   
2. `cd target_dir`  
3. create the Python Virtual Environment with `python3.x -m venv .`  
4. activate it with `source bin/activate`  
5. run `pip install -r requirements.txt` to install required libraries.  
6. set up your contrab to run `python xmpp.py` every minute.  

Enjoy!  

3.4.2024 - total refactor, adding realtime replies thanks to Mastodon's Streaming API  
4.4.2024 - save registered user id, username, created_at and service to Postgresql database  
8.4.2024 - check Mastodon server activity, delete from service Mastodon's inactive users.

