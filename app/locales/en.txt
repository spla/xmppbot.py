register_str: register
unregister_str: unregister
stats_str: stats
status_str: status
registerok_str: xmpp account registered!
user_str: user:
password_str: password:
server_str: server:
xmpp_account_str: xmpp account
notdeleteadmin_str: you are admin, can't delete your account!
deleted_str: deleted succesfully!
stats_title_str: #xmpp node stats at
registered_users_str: registered users:
users_online_str: online users:
node_users_str: node users:
uptime_str: uptime:
processes_str: processes:
account_exists_str: account already exists!!
user_sessions_info_str: sessions
current_sessions_str: current sessions:
sessions_connection_str: connections:
sessions_ip_str: IP:
sessions_port_str: port: 
sessions_priority_str: priority:
sessions_node_str: node:
sessions_uptime_str: uptime:
sessions_status_str: status:
sessions_resource_str: resource:
sessions_statustext_str: status text:



