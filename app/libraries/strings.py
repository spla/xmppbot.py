import os
import sys
import os.path
import pdb

class Strings:

    name = 'xmppbot strings'

    def __init__(self, bot_lang_path=None, bot_lang=None):

        self.bot_lang_path = "config/lang.txt"

        is_setup = self.check_setup(self)

        if is_setup:

            self.bot_lang = self.get_parameter("bot_lang", self.bot_lang_path)
            self.load_strings(self)

        else:

            self.bot_lang = self.setup(self)

        self.load_strings(self)

    @staticmethod
    def check_setup(self):

        is_setup = False

        if not os.path.isfile(self.bot_lang_path):
            print(f"File {self.bot_lang_path} not found, running setup.")
            return
        else:
            is_setup = True
            return is_setup

    @staticmethod
    def setup(self):

        self.bot_lang = input("Bot replies lang (ca or en)? ")

        if not os.path.exists(self.bot_lang_path):
            with open(self.bot_lang_path, 'w'): pass
            print(f"{self.bot_lang_path} created!")

        with open(self.bot_lang_path, 'a') as the_file:
            print(f"Writing bot lang parameters to {self.bot_lang_path}")
            the_file.write(f'bot_lang: {self.bot_lang}')

        return self.bot_lang

    def get_parameter(self, parameter, file):

        with open( file ) as f:
            for line in f:
                if line.startswith( parameter ):
                    return line.replace(parameter + ":", "").strip()

        print(f'{self.bot_path} Missing parameter {parameter}')
        sys.exit(0)

    @staticmethod
    def load_strings(self):

        lang_file_path = f"app/locales/{self.bot_lang}.txt"

        self.register_str = self.get_parameter("register_str", lang_file_path)
        self.unregister_str =  self.get_parameter("unregister_str", lang_file_path)
        self.stats_str = self.get_parameter("stats_str", lang_file_path)
        self.status_str = self.get_parameter("status_str", lang_file_path)
        self.registerok_str = self.get_parameter("registerok_str", lang_file_path)
        self.user_str = self.get_parameter("user_str", lang_file_path)
        self.password_str = self.get_parameter("password_str", lang_file_path)
        self.change_password_str = self.get_parameter("change_password_str", lang_file_path)
        self.server_str = self.get_parameter("server_str", lang_file_path)
        self.xmpp_account_str = self.get_parameter("xmpp_account_str", lang_file_path)
        self.notdeleteadmin_str = self.get_parameter("notdeleteadmin_str", lang_file_path)
        self.deleted_str = self.get_parameter("deleted_str", lang_file_path)
        self.stats_title_str = self.get_parameter("stats_title_str", lang_file_path) 
        self.registered_users_str = self.get_parameter("registered_users_str", lang_file_path)
        self.users_online_str = self.get_parameter("users_online_str", lang_file_path)
        self.node_users_str = self.get_parameter("node_users_str", lang_file_path)
        self.uptime_str = self.get_parameter("uptime_str", lang_file_path)
        self.processes_str = self.get_parameter("processes_str", lang_file_path)
        self.account_exists_str = self.get_parameter("account_exists_str", lang_file_path)
        self.user_sessions_info_str = self.get_parameter("user_sessions_info_str", lang_file_path)
        self.current_sessions_str = self.get_parameter("current_sessions_str", lang_file_path)
        self.sessions_connection_str = self.get_parameter("sessions_connection_str", lang_file_path)
        self.sessions_ip_str = self.get_parameter("sessions_ip_str", lang_file_path)
        self.sessions_port_str = self.get_parameter("sessions_port_str", lang_file_path) 
        self.sessions_priority_str = self.get_parameter("sessions_priority_str", lang_file_path)
        self.sessions_node_str = self.get_parameter("sessions_node_str", lang_file_path)
        self.sessions_uptime_str = self.get_parameter("sessions_uptime_str", lang_file_path)
        self.sessions_status_str = self.get_parameter("sessions_status_str", lang_file_path)
        self.sessions_resource_str = self.get_parameter("sessions_resource_str", lang_file_path)
        self.sessions_statustext_str = self.get_parameter("sessions_statustext_str", lang_file_path)
