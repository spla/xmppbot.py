import unidecode
import re
import pdb

class Mentions:

    name = 'Mentions'

    def __init__(self, mastodon=None):

        self.mastodon = mastodon

    def get_data(self, mention):

        notification_id = mention.id

        account_id = mention.account.id

        username = mention.account.acct

        status_id = mention.status.id

        text  = mention.status.content

        visibility = mention.status.visibility

        reply, query_word = self.get_question(username, text)

        return reply, query_word, username, status_id, visibility

    def get_question(self, username, text):

        reply = False

        keyword = '' 

        if '@' in username:

            return reply, keyword

        content = self.cleanhtml(text)

        content = self.unescape(content)

        try:

            start = content.index("@")
            end = content.index(" ")
            if len(content) > end:

                content = content[0: start:] + content[end +1::]

            cleanit = content.count('@')

            i = 0
            while i < cleanit :

                start = content.rfind("@")
                end = len(content)
                content = content[0: start:] + content[end +1::]
                i += 1

            question = content.lower()

            query_word = question

            if query_word == 'registre' or query_word == 'nova contrasenya' or query_word == 'baixa' or query_word == 'info' or query_word == 'estat' or query_word == 'sessions':

                reply = True

            return (reply, query_word)

        except:

            pass

    @staticmethod
    def cleanhtml(raw_html):
        cleanr = re.compile('<.*?>')
        cleantext = re.sub(cleanr, '', raw_html)
        return cleantext

    @staticmethod
    def unescape(s):
        s = s.replace("&apos;", "'")
        s = s.replace("&#39;", "'")
        s = s.replace("&quot;", '"')
        return s
